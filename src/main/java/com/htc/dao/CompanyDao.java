package com.htc.dao;

import java.util.List;

import com.htc.exception.DataException;
import com.htc.model.CompanyEntity;

public interface CompanyDao {
	public boolean insert(CompanyEntity c) throws DataException ;
	public CompanyEntity getCompanyById(Integer id);
	public List<CompanyEntity> getCompanies();
}
