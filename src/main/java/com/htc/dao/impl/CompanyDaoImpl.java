package com.htc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.htc.dao.CompanyDao;
import com.htc.exception.DataException;
import com.htc.model.CompanyEntity;
import com.htc.model.EmployeeEntity;
import com.htc.util.AppConstants;

@Component
public class CompanyDaoImpl implements CompanyDao {

	private List<CompanyEntity> companies;
	
	@PostConstruct
	public void initialize() {
		companies = new ArrayList<>();
		List<EmployeeEntity> employees = new ArrayList<>();
		employees.add(new EmployeeEntity(1,"Empleado 1","Puesto 1"));
		employees.add(new EmployeeEntity(2,"Empleado 2","Puesto 2"));
		employees.add(new EmployeeEntity(3,"Empleado 3","Puesto 3"));
		companies.add(new CompanyEntity(1, "Empresa 1", employees));
		employees.clear();
		employees.add(new EmployeeEntity(4,"Empleado 4","Puesto 4"));
		employees.add(new EmployeeEntity(5,"Empleado 5","Puesto 5"));
		employees.add(new EmployeeEntity(6,"Empleado 6","Puesto 6"));
		companies.add(new CompanyEntity(2, "Empresa 2", employees));
	}
	
	@Override
	public boolean insert(CompanyEntity c) throws DataException {
		for (CompanyEntity companyEntity : companies) {
			if(companyEntity.getId()== c.getId() ) {
				System.out.println("Error id duplicado "+c.getId());
				throw new DataException(AppConstants.CODE_INSERT,"Error id duplicado "+c.getId());
			}
		}
		return companies.add(c);
	}

	@Override
	public CompanyEntity getCompanyById(Integer id) {
		for (CompanyEntity companyEntity : companies) {
			if(companyEntity.getId()==id) return companyEntity;
		}
		return null;
	}

	@Override
	public List<CompanyEntity> getCompanies() {
		return companies;
	}

}
