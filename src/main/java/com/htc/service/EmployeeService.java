package com.htc.service;

import com.htc.company.Employee;
import com.htc.exception.InputRequestException;
import com.htc.model.EmployeeEntity;

public interface EmployeeService {
	public Employee toDto(EmployeeEntity mEmployee);
	public EmployeeEntity toEntity(Employee rEmployee) throws InputRequestException;
}
