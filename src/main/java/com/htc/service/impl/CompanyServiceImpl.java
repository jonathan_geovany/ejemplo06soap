package com.htc.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.company.Company;
import com.htc.company.Employee;
import com.htc.dao.CompanyDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.CompanyEntity;
import com.htc.model.EmployeeEntity;
import com.htc.service.CompanyService;
import com.htc.service.EmployeeService;
import com.htc.util.AppConstants;

@Component
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private EmployeeService employeeService;
	
	@Override
	public boolean insert(CompanyEntity c) throws AbstractException{
		return companyDao.insert(c);
	}

	@Override
	public CompanyEntity getCompanyById(Integer id) throws ServiceException {
		CompanyEntity companyEntity = companyDao.getCompanyById(id);
		if(companyEntity==null) throw new ServiceException(AppConstants.CODE_NOT_FOUND, AppConstants.MSG_NOT_FOUND);
		return companyDao.getCompanyById(id);
	}

	@Override
	public List<CompanyEntity> getCompanies() throws ServiceException{
		List<CompanyEntity> companies = companyDao.getCompanies();
		if(companies==null) throw new ServiceException(AppConstants.CODE_NOT_FOUND, AppConstants.MSG_NOT_FOUND);
		return companies;
	}

	@Override
	public Company toDto(CompanyEntity mCompany) {
		Company rCompany = new Company();
		rCompany.setId(mCompany.getId());
		rCompany.setName(mCompany.getName());
		for (EmployeeEntity mEmployee : mCompany.getEmployees()) {
			rCompany.getEmployees().add(employeeService.toDto(mEmployee));
		}
		return rCompany;
	}

	@Override
	public CompanyEntity toEntity(Company rCompany) throws InputRequestException{
		if(rCompany==null || rCompany.getName()==null || rCompany.getName().isEmpty() || rCompany.getEmployees()==null)
			throw new InputRequestException(AppConstants.CODE_NULL_EMPTY, AppConstants.MSG_NULL_EMPTY_COMPANY);
		if(rCompany.getEmployees().isEmpty())
			throw new InputRequestException(AppConstants.CODE_EMPTY_LIST, AppConstants.MSG_EMPTY_LIST_EMPLOYEE);
		List<EmployeeEntity> mEmployees = new ArrayList<>();
		for (Employee rEmployee : rCompany.getEmployees()) {
			mEmployees.add(employeeService.toEntity(rEmployee));
		}
		return new CompanyEntity(rCompany.getId(),rCompany.getName(),mEmployees);
	}

	@Override
	public List<Company> toDtoList(List<CompanyEntity> rCompanies) {
		List<Company> companies=new ArrayList<>();
		for (CompanyEntity companyEntity : rCompanies) {
			companies.add(toDto(companyEntity));
		}
		return companies;
	}

}
