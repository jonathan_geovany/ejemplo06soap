package com.htc.service.impl;

import org.springframework.stereotype.Component;

import com.htc.company.Employee;
import com.htc.exception.InputRequestException;
import com.htc.model.EmployeeEntity;
import com.htc.service.EmployeeService;
import com.htc.util.AppConstants;

@Component
public class EmployeeServiceImpl implements EmployeeService {
	
	@Override
	public Employee toDto(EmployeeEntity mEmployee) {
		Employee rEmployee = new Employee();
		rEmployee.setId(mEmployee.getId());
		rEmployee.setName(mEmployee.getName());
		rEmployee.setPosition(mEmployee.getPosition());
		return rEmployee;
	}

	@Override
	public EmployeeEntity toEntity(Employee rEmployee) throws InputRequestException{
		if(rEmployee==null || rEmployee.getName()==null || rEmployee.getName().isEmpty() || rEmployee.getPosition()==null || rEmployee.getPosition().isEmpty()) {
			System.out.println("antes de exception");
			throw new InputRequestException(AppConstants.CODE_NULL_EMPTY, AppConstants.MSG_NULL_EMPTY_EMPLOYEE);
		}			
		System.out.println("Hola llegue aqui");
		return new EmployeeEntity(rEmployee.getId(),rEmployee.getName(),rEmployee.getName());
	}

}
