package com.htc.service;

import java.util.List;

import com.htc.company.Company;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.CompanyEntity;

public interface CompanyService {
	public Company toDto(CompanyEntity mCompany);
	public List<Company> toDtoList(List<CompanyEntity> rCompanies);
	public CompanyEntity toEntity(Company rCompany) throws InputRequestException;
	public boolean insert(CompanyEntity c) throws AbstractException;
	public CompanyEntity getCompanyById(Integer id) throws ServiceException;
	public List<CompanyEntity> getCompanies() throws ServiceException;
}
