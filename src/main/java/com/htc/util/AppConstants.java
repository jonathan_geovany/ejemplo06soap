package com.htc.util;

public final class AppConstants {

	
	
	/*** codigo default ***/
	public static final long DEFAULT_OK_CODE=200L;
	public static final long DEFAULT_ERROR_CODE = 400L;
	
	//mensaje de ok
	public static final String OK_INSERT = "Registro guardado";
	public static final String OK_FOUND = "Registro encontrado";
	
	/*** codigos de error con mensajes ***/
	
	//de entrada del usuario
	
		//para parametros nulos o vacios
	public static final long CODE_NULL_EMPTY = 400L;
	public static final String MSG_NULL_EMPTY_COMPANY = "Company contiene datos nulos o vacios";
	public static final String MSG_NULL_EMPTY_EMPLOYEE = "Employee contiene datos nulos o vacios";
		//para parametros nulos o vacios
	public static final long CODE_EMPTY_LIST= 401L;
	public static final String MSG_EMPTY_LIST_EMPLOYEE = "Lista de empleados esta vacia";
	
	//del la capa service o dao
	public static final long CODE_INSERT= 500L;
	public static final String MSG_INSERT = "No se pudo guardar en la base de datos";
	
	public static final long CODE_NOT_FOUND= 500L;
	public static final String MSG_NOT_FOUND = "No se encontro el registro";
	
	
}
