package com.htc.model;

import java.io.Serializable;
import java.util.List;

public class CompanyEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private List<EmployeeEntity> employees;
	public CompanyEntity() {
		super();
	}
	public CompanyEntity(Integer id, String name, List<EmployeeEntity> employees) {
		super();
		this.id = id;
		this.name = name;
		this.employees = employees;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<EmployeeEntity> getEmployees() {
		return employees;
	}
	public void setEmployees(List<EmployeeEntity> employees) {
		this.employees = employees;
	}
	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + "]";
	}
	
}
