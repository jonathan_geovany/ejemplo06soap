package com.htc.exception;

public abstract class AbstractException extends Exception {
	private static final long serialVersionUID = 1L;
	private static final long DEFAULT_CODE = 400;
	private final long code;
	private final String description;

	public AbstractException(Exception ex) {
		super(ex);
		this.code = DEFAULT_CODE;		
		description = ex.getMessage();
	}

	public AbstractException(long code, String description,Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}

	public AbstractException(long code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileDataException [code=").append(code).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}
	
	
}
