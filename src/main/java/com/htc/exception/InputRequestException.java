package com.htc.exception;

public class InputRequestException extends AbstractException{
	private static final long DEFAULT_CODE_EXCEPTION = 400;
	private static final long serialVersionUID = 1L;
	public InputRequestException(Exception ex) {
		super(ex);
	}
	public InputRequestException(long code, String description, Exception ex) {
		super(code, description, ex);
	}
	public InputRequestException(long code, String description) {
		super(code, description);
	}
	public InputRequestException(String description, Exception ex) {
		super(DEFAULT_CODE_EXCEPTION,description, ex);
	}
}
