package com.htc.exception;

public class DataException extends AbstractException {
	private static final long DEFAULT_CODE_EXCEPTION = 500;
	private static final long serialVersionUID = 1L;
	public DataException(Exception ex) {
		super(ex);
	}
	public DataException(long code, String description, Exception ex) {
		super(code, description, ex);
	}
	public DataException(long code, String description) {
		super(code, description);
	}
	public DataException(String description, Exception ex) {
		super(DEFAULT_CODE_EXCEPTION,description, ex);
	}
}
