package com.htc.ws.endpoint;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.company.Company;
import com.htc.company.GetCompaniesRequest;
import com.htc.company.GetCompaniesResponse;
import com.htc.company.GetCompanyByIdRequest;
import com.htc.company.GetCompanyByIdResponse;
import com.htc.company.InsertCompanyRequest;
import com.htc.company.InsertCompanyResponse;
import com.htc.company.Result;
import com.htc.exception.AbstractException;
import com.htc.exception.ServiceException;
import com.htc.service.CompanyService;
import com.htc.util.AppConstants;

@Endpoint
public class CompanyEndpoint {
	private static final String NAMESPACE_URI = "http://company.htc.com";
	
	private CompanyService companyService;

	@Autowired
	public CompanyEndpoint(CompanyService companyService) {
		super();
		this.companyService = companyService;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertCompanyRequest")
	@ResponsePayload
	public InsertCompanyResponse insertCompany(@RequestPayload InsertCompanyRequest request) {
		InsertCompanyResponse response = new InsertCompanyResponse();
		Result result = new Result();
		try {
			if(!companyService.insert(companyService.toEntity(request.getCompany()))) {
				throw new ServiceException(AppConstants.CODE_INSERT,AppConstants.MSG_INSERT);
			}
			result.setCode(AppConstants.DEFAULT_OK_CODE);
			result.setMsg(AppConstants.OK_INSERT);
		} 
		catch (AbstractException e) {
			result.setCode(e.getCode());
			result.setMsg(e.getDescription());
		}
		catch (Exception e) {
			result.setCode(AppConstants.DEFAULT_ERROR_CODE);
			result.setMsg(e.getMessage());
		}
		response.setResult(result);
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCompanyByIdRequest")
	@ResponsePayload
	public GetCompanyByIdResponse getCompany(@RequestPayload GetCompanyByIdRequest request) {
		GetCompanyByIdResponse response = new GetCompanyByIdResponse();
		Result result = new Result();
		try {
			Company company = companyService.toDto(companyService.getCompanyById(request.getId()));
			response.setCompany(company);
			result.setCode(AppConstants.DEFAULT_OK_CODE);
			result.setMsg(AppConstants.OK_FOUND);
		} 
		catch (AbstractException e) {
			result.setCode(e.getCode());
			result.setMsg(e.getDescription());
		}
		catch (Exception e) {
			result.setCode(AppConstants.DEFAULT_ERROR_CODE);
			result.setMsg(e.getMessage());
		}
		response.setResult(result);
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCompaniesRequest")
	@ResponsePayload
	public GetCompaniesResponse getCompanies(@RequestPayload GetCompaniesRequest request) {
		GetCompaniesResponse response = new GetCompaniesResponse();
		Result result = new Result();
		try {
			List<Company> companies = companyService.toDtoList(companyService.getCompanies());
			response.getCompanies().addAll(companies);
			result.setCode(AppConstants.DEFAULT_OK_CODE);
			result.setMsg(AppConstants.OK_FOUND);
		} 
		catch (AbstractException e) {
			result.setCode(e.getCode());
			result.setMsg(e.getDescription());
		}
		catch (Exception e) {
			result.setCode(AppConstants.DEFAULT_ERROR_CODE);
			result.setMsg(e.getMessage());
		}
		response.setResult(result);
		return response;
	}
	
}
