//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.25 a las 02:39:50 PM CST 
//


package com.htc.company;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.company package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.company
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertCompanyResponse }
     * 
     */
    public InsertCompanyResponse createInsertCompanyResponse() {
        return new InsertCompanyResponse();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetCompanyByIdResponse }
     * 
     */
    public GetCompanyByIdResponse createGetCompanyByIdResponse() {
        return new GetCompanyByIdResponse();
    }

    /**
     * Create an instance of {@link Company }
     * 
     */
    public Company createCompany() {
        return new Company();
    }

    /**
     * Create an instance of {@link GetCompaniesRequest }
     * 
     */
    public GetCompaniesRequest createGetCompaniesRequest() {
        return new GetCompaniesRequest();
    }

    /**
     * Create an instance of {@link GetCompanyByIdRequest }
     * 
     */
    public GetCompanyByIdRequest createGetCompanyByIdRequest() {
        return new GetCompanyByIdRequest();
    }

    /**
     * Create an instance of {@link InsertCompanyRequest }
     * 
     */
    public InsertCompanyRequest createInsertCompanyRequest() {
        return new InsertCompanyRequest();
    }

    /**
     * Create an instance of {@link GetCompaniesResponse }
     * 
     */
    public GetCompaniesResponse createGetCompaniesResponse() {
        return new GetCompaniesResponse();
    }

    /**
     * Create an instance of {@link Employee }
     * 
     */
    public Employee createEmployee() {
        return new Employee();
    }

}
